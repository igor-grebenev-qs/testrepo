package test

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestInit(t *testing.T) {
	err := Init()
	require.Empty(t, err)
}

func TestInit2(t *testing.T) {
	err := Init()
	require.NotEmpty(t, err)
}

/*
func TestFailInit(t *testing.T) {
	err := Init()
	assert.NotEmpty(t, err)
}
*/